#Codice per la creazione di defs, dato un database completo di Header+Packet+Tail
import pandas as pd                         #Libreria per la scrittura e lettura dei file .csv
from collections import OrderedDict

if __name__ == '__main__':
    df = pd.read_csv('SSAT_Mnemonics.csv')          #Andiamo a leggere tutto il file .csv
    ind = df.columns
    print(ind)
    apids = set(df['packet_display_name'])          #Associamo alla variabile apids il numero della colonna 'packet_display_name' all'interno del file .csv (puntiamo alle cartelle apid001 ecc) però lo prende una volta togliendo i duplicati
    print (apids)

    for apid in apids:
        mask = (df['packet_display_name'] == apid)  #Creiamo una maschera che mi evidenzi dal file solo il numero apid settata senza i duplicati
        df_s = df[mask]
        
        defs = OrderedDict([
            ('name', []),
            ('data_type', []),
            ('bit_offset', []),
            ('bit_length', []),
            ('calibration', [])
        ])
        #OrderedDict è un oggetto della libreria collections e 
        #tiene memoria di come vengono inserite le chiavi che sono i valori della prima riga del file .csv (name,data_type,bit_offset,bit_length,calibration)

        for i, row in df_s.iterrows():
            #Ciclo per saltare l'header primario
            if row['mnemonic'].split('_', 1)[-1].upper() in (
                     'PKTVNO', 'PCKT', 'SHDF',
                     'APID', 'SEGF', 'CNT', 'PLEN'
            ):
                continue                                         #Tale ciclo se nella colononna 'mnemonic' torva una chiave di quelle indicate, salta la riga e prosegue oltre (salta le prime 8 righe costituenti il primary header)
            
            name = row['mnemonic'].lower()
            data_type = row['#datatype'].lower()
            bit_offset = (row['bit_offset'] - 6)*8 + row['bit_offset']             #ECCSS-E-ST-70-41C15 pag. 438: al bit_offset togglie le righe del file da 2 a 8, moltiplicandole per il numero di bit e poi continua la somma    
            bit_length = row['bit_length']
            calibration = row['mnemonic_calibration_values']

            if data_type == 'char':
                data_type == 'str'

            dim = row['dimension']

            if dim == 1:
                defs['name'].append(name)
                defs['data_type'].append(data_type)
                defs['bit_offset'].append(bit_offset)
                defs['bit_length'].append(bit_length)
                defs['calibration'].append(calibration)
            else:
                if data_type != 'float':
                    bit_length = int(bit_length/dim)

                    for j in range(dim):
                        defs['name'].append('{}[{}]'.format(name,j))
                        defs['data_type'].append(data_type)
                        defs['bit_offset'].append(bit_offset)
                        defs['bit_length'].append(bit_length)
                        defs['calibration'].append(calibration)

        out_filename = 'apid{:03d}/defs.csv'.format(apid)
        pd.DataFrame(defs).to_csv(out_filename, index=0)
        print
