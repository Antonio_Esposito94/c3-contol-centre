#Test per la decodifica di un pacchetto di lunghezza fissata.
import csv
import glob                         #Il modulo glob fornisce funzioni per creare liste di file che provengono da directory sulle quali è stata effettuata una ricerca con caratteri wildcard (crea una lista di file contenuti nella dirctory)
import json                         #Libreria di lettura e scrittura di file
import os
from collections import OrderedDict
import numpy as np
from ccsdspy import FixedLength, PacketField



def run_apid_test(apid):
    """Funzione per lanciare il file APID. Esso contine:
                defs.csv -- pacchetto definizioni con conversioni
                .tlm     -- binary CCSDS file
                .cvt.csv -- file csv contenente i valori dopo la conversione del file CCSDS
    """
    global defs
    global truth
    global decoded                              #con global riesco a vedere la variabile anche al livello superiore
    #Definizione del percorso dei file nella cartella apid_Prova
    dir_path = os.path.dirname(os.path.realpath(__file__))      #Tale istruzione restituisce il percorso (directory da dove viene lanciato il file .py)
                                                                #Ex(C:\test\esempi la variabile assumerà il valore (C:\\test\\esempi))
    
    apid_dir = os.path.join(dir_path,'Data','apid{:03d}'.format(apid)) #Tale istruzione concatena al valore della variabile dir_path, la stringa 'apid_prova'
                                                                       #la variabile avrà dunque il valore (C:\\test\\esempi\\apid_prova) puntando così al file che si vuole leggere
                                                                       #il comando '{:03d}' mi dice che il formato della cartella sarà apid+3numeri
    
    defs_file_path = os.path.join(apid_dir,'defs.csv')
    truth_file_path = glob.glob(os.path.join(apid_dir,'*.cvt.csv')).pop()
    ccsds_file_path = glob.glob(os.path.join(apid_dir,'*.tlm')).pop()         #pop rimuove un oggetto dalla lista (prendiamo il file che ci serve)
    

    assert all(os.path.exists(path) for path in(
        apid_dir,
        defs_file_path,
        truth_file_path,
        ccsds_file_path,
    ))

    #Carichiamo il dato convertito (nel formato CSV), e decodifichiamo il file
    defs = load_apid_defs(defs_file_path)
    truth = load_apid_truth(truth_file_path,defs)
    decoded = decode_ccsds_file(ccsds_file_path,defs)

def load_apid_truth(truth_file_path,defs):
    """Carica  il file CSV con i valori veri e restituisce una tabella"""
    with open(truth_file_path) as fh:           #Apre il file .csv e gli assegna l'alias a cui farà riferimento per la lettura
        lines = fh.readlines()

    colnames = lines[0][:-1].split(',')                                 #Spaccotta la prima riga riconoscendo come separatore la virgola
    table_dict = OrderedDict([(colname,[]) for colname in colnames])    #Definisce le chiavi del dizionario

    with open(truth_file_path) as fh:
        reader = csv.reader(fh) #Legge il contenuto del file
        """ nello spacchettamento delle righe del file, bisogna escludere la prima
            riga che contiene le definizioni delle chiavi"""
        first_line=True                        #Flag per saltare la prima riga
        for row in reader:                     #Lettura delle righe
            if first_line:
                first_line = False
                continue                       #Se è la prima riga non fare nulla, e quando è stata letta cambia il flag, passa al secondo elemento

            for key,row_value in zip(table_dict.keys(),row):         #Il comando zip consente di far muovere contemporaneamente gli indici in più liste: keys si muove in table_dict.keys() e row_value in row
                table_dict[key].append(row_value)                    #Questa istruzione associa i valori alle chiavi del dizionario nel seguente modo:
                                                                     #Alla chiave name associa la tupla prima colonna del file
                                                                     #Alla chiave data_type associa la tupla seconda colonna del file
                                                                     #Alla chiave bit_length associa la tupla terza colonna
                                                                     #Alla chiave calibration associa la tupla quarta colonna

    #Lasciamo le colonne che non ci servono. Prendiamo solo quelle da decodificare prese da defs
    keep_cols = set(defs['name'])
    for colname in colnames:
        if colname not in keep_cols:
            del table_dict[colname]

    #Settiamo i tipi corretti usando il dizionario defs
    for key, data_type, cal in zip(defs["name"],defs["data_type"],defs["calibration"]):

        if cal:
            dtype = np.array(cal.values()).dtype
            table_dict[key] = np.array(table_dict[key],dtype=dtype)
        elif data_type == 'uint':
            table_dict[key] = np.array(table_dict[key],dtype=np.uint)
        elif data_type == 'int':
            table_dict[key] = np.array(table_dict[key],dtype=np.int)
        elif data_type == 'str':
            table_dict[key] = np.array(table_dict[key],dtype=str)
        elif data_type == 'float':
            table_dict[key] = np.array(table_dict[key],dtype=float)
        else:
            raise RuntimeError('Type {} implemented'.format(data_type))

    return table_dict


def load_apid_defs(defs_file_path):
    """Carica le definizioni APID (defs.csv) e restituisce una tabella"""
    table_dict = OrderedDict([
        ('name',[]),
        ('data_type',[]),
        ('bit_offset',[]),
        ('bit_length',[]),
        ('calibration',[])
    ])

    #Tale funzione usa un dizionario per la lettura del file e ne definisce le chiavi. OrderedDict è un oggetto della libreria collections e 
    #tiene memoria di come vengono inserite le chiavi che sono i valori della prima riga del file .csv
    #Definiamo le chiavi:

    with open(defs_file_path) as fh:
        reader = csv.reader(fh)     #Legge il contenuto del file
        """ nello spaccottamento delle righe del file, bisogna escludere la prima
            riga che contiene le definizioni delle chiavi"""
        first_line = True           #Flag per saltare la prima riga
        for row in reader:                     #Lettura delle righe
            if first_line:
                first_line = False
                continue                       #Se è la prima riga non fare nulla, e quando è stata letta cambia il flag, passa al secondo elemento

            for key, row_value in zip(table_dict.keys(), row):
                table_dict[key].append(row_value)

    #Cambio da stringa a valore finale
    table_dict['name'] = [name.upper() for name in table_dict['name']]
    table_dict['bit_offset'] = [int(n) for n in table_dict['bit_offset']]
    table_dict['bit_length'] = [int(n) for n in table_dict['bit_length']]

    decode_cal = lambda cal: json.loads(cal) if cal else None
    table_dict['calibration'] = [decode_cal(v) for v in table_dict['calibration']]

    return table_dict


def decode_ccsds_file(ccsds_file_path, defs):
    pkt_fields = []

    for key, data_type, bit_length in zip(defs["name"], defs["data_type"], defs["bit_length"]):
        pkt_fields.append(
            PacketField(name=key,
                          data_type=data_type,
                          bit_length=bit_length)
        )
    
    pkt = FixedLength(pkt_fields)
    decoded = pkt.load(ccsds_file_path)

    return decoded


#def test_hs_apid001():
run_apid_test(2)
    
def test_hs_apid010():
    run_apid_test(10)

def test_hs_apid035():
    run_apid_test(35)

def test_hs_apid130():
    run_apid_test(130)

def test_hs_apid251():
    run_apid_test(251)

def test_hs_apid895():
    run_apid_test(895)
